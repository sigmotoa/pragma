package com.sigmotoa.gatitos.Blocks

import com.sigmotoa.gatitos.Models.Cat
import retrofit2.Response

import retrofit2.http.GET
import retrofit2.http.Header

/**
 *
 * Created by sigmotoa on 22/04/23.
 * @author sigmotoa
 *
 * www.sigmotoa.com
 */
interface CatApi {

    @GET("/v1/breeds")
    suspend fun getCats(
        @Header("x-api-key") apiKey: String,
    ):Response<List<Cat>>

}