package com.sigmotoa.gatitos.Views.components

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.sigmotoa.gatitos.Blocks.CatApi
import com.sigmotoa.gatitos.Data.RetrofitHelper
import com.sigmotoa.gatitos.Models.Cat
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.job
import kotlinx.coroutines.withContext

/**
 *
 * Created by sigmotoa on 22/04/23.
 * @author sigmotoa
 *
 * www.sigmotoa.com
 */

@Composable
fun GatitosList() {
    var gatitos by remember { mutableStateOf<List<Cat>>(emptyList()) }

    LaunchedEffect(Unit) {
        try {
            val response = RetrofitHelper.getRetrofit().getCats("bda53789-d59e-46cd-9bc4-2936630fde39")
            if (response.isSuccessful) {
                gatitos = response.body() ?: emptyList()
            } else {
                Log.e("GatitosList", "Error al obtener gatitos: ${response.message()}")
            }
        } catch (e: Exception) {
            Log.e("GatitosList", "Error al obtener gatitos", e)
        }
    }

    LazyColumn {
        items(gatitos.size) { gatito ->
            Card(
                shape = RoundedCornerShape(8.dp),
                modifier = Modifier.padding(16.dp),

            ) {
                Column(
                    modifier = Modifier.fillMaxWidth()
                ) {
                    /*Image(
                        painter = rememberImagePainter(gatitos.get(gatito).imageUrl),
                        contentDescription = null,
                        modifier = Modifier.fillMaxWidth()
                    )*/
                    Text(
                        text = gatitos.get(gatito).breedName,
                        fontSize = 20.sp,
                        fontWeight = FontWeight.Bold,
                        textAlign = TextAlign.Center,
                        modifier = Modifier.padding(16.dp)
                    )
                    Text(
                        text = "Origen: ${gatitos.get(gatito).origin}",
                        fontSize = 16.sp,
                        fontWeight = FontWeight.Normal,
                        textAlign = TextAlign.Center,
                        modifier = Modifier.padding(bottom = 8.dp)
                    )
                    Text(
                        text = "Nivel de afecto: ${gatitos.get(gatito).affection_level}",
                        fontSize = 16.sp,
                        fontWeight = FontWeight.Normal,
                        textAlign = TextAlign.Center,
                        modifier = Modifier.padding(bottom = 8.dp)
                    )
                    Text(
                        text = "Nivel de inteligencia: ${gatitos.get(gatito).intelligence}",
                        fontSize = 16.sp,
                        fontWeight = FontWeight.Normal,
                        textAlign = TextAlign.Center,
                        modifier = Modifier.padding(bottom = 16.dp)
                    )
                }
            }
        }
    }
}
