package com.sigmotoa.gatitos.Models

data class Cat(
    val breedName: String,
    val origin: String,
    val affection_level: String,
    val intelligence: Int,
    val imageUrl: String
)
